package br.com.itau.Auditoria.service;

import br.com.itau.Auditoria.model.Receita;
import br.com.itau.Auditoria.Client.ReceitaClient;
import br.com.itau.Auditoria.model.Cadastro;
import br.com.itau.Auditoria.producer.ValidacaoProducer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ValidacaoService {

    @Autowired
    private ReceitaClient receitaClient;

    @Autowired
    private ValidacaoProducer validacaoProducer;

    public Receita consultarReceita(Cadastro cadastro){
        Receita objReceita = receitaClient.buscaCnpj(cadastro.getCnpj());

        if (verificaCapital(objReceita.getCapitalSocial()).equals(true)){
            validacaoProducer.enviarMsgKafka(objReceita);
        }

        return objReceita;
    }

    public Boolean verificaCapital(Double capitalSocial){
        if (capitalSocial >= 1000000){
            return true;
        }
        return false;
    }
}

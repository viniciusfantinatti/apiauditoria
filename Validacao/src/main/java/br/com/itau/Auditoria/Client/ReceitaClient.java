package br.com.itau.Auditoria.Client;

import br.com.itau.Auditoria.model.Receita;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient(name = "receita", url = "https://receitaws.com.br/")
public interface ReceitaClient {

    @GetMapping("v1/cnpj/{cnpj}")
    Receita buscaCnpj(@PathVariable String cnpj);
}

package br.com.itau.Auditoria.consumer;

import br.com.itau.Auditoria.model.Cadastro;
import br.com.itau.Auditoria.service.ValidacaoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Component;

@Component
public class CadastroConsumer {

    @Autowired
    private ValidacaoService validacaoService;

    @KafkaListener(topics = "spec3-vinicius-fantinatti-2", groupId = "ViniFantBR-1")
    public void receberMsgKafka(@Payload Cadastro cadastro){
        System.out.println("CNPJ: " + cadastro.getCnpj());
        validacaoService.consultarReceita(cadastro);
    }

}

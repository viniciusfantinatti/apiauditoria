package br.com.itau.Auditoria.producer;

import br.com.itau.Auditoria.model.Receita;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

@Service
public class ValidacaoProducer {
    @Autowired
    private KafkaTemplate<String, Receita> producer;

    public void enviarMsgKafka(Receita receita){
        producer.send("spec3-vinicius-fantinatti-3", receita);
    }

}

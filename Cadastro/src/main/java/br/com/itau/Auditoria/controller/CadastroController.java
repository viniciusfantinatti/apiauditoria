package br.com.itau.Auditoria.controller;

import br.com.itau.Auditoria.model.Cadastro;
import br.com.itau.Auditoria.producer.CadastroProducer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class CadastroController {

    @Autowired
    private CadastroProducer cadastroProducer;

    @PostMapping("/cadastro")
    public void cadastrar(@RequestBody Cadastro cadastro){
        cadastroProducer.enviarMsgKafka(cadastro);
    }
}

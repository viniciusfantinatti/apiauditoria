package br.com.itau.Auditoria.producer;

import br.com.itau.Auditoria.model.Cadastro;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

@Service
public class CadastroProducer {

    @Autowired
    private KafkaTemplate<String, Cadastro> producer;

    public void enviarMsgKafka(Cadastro cadastro){
        producer.send("spec3-vinicius-fantinatti-2", cadastro);
    }
}

package br.com.itau.Auditoria.consumer;

import br.com.itau.Auditoria.model.Receita;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Component;

import java.io.FileWriter;
import java.io.IOException;

@Component
public class EfetivacaoConsumer {

    @KafkaListener(topics = "spec3-vinicius-fantinatti-3", groupId = "ViniFantBR-2")
    public void receber(@Payload Receita receita){
        gerarArquivoLog("/home/a2w/Workspace/Vinicius/Especializacao/Cadastro.txt", receita);
        System.out.println("LOG: " + receita.getCnpj() + "|" + receita.getNome() + "|" + receita.getFantasia() + "|" +
                receita.getCapitalSocial().doubleValue() +"|"+ receita.getCapitalSocial().longValue());
    }

    private static void gerarArquivoLog(String nomeArq, Receita receita){
        try{
            FileWriter writer = new FileWriter(nomeArq, true);

            writer.write(receita.getNome());
            writer.write(';');
            writer.write(receita.getFantasia());
            writer.write(';');
            writer.write(receita.getCnpj());
            writer.write(';');
            writer.write(receita.getCapitalSocial().toString());
            writer.write('\n');

            writer.flush();
            writer.close();

        }catch (IOException e){
            e.printStackTrace();
        }
    }
}

package br.com.itau.Auditoria.model;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Receita {


    private String nome;
    private String fantasia;
    private String cnpj;
    @JsonProperty("capital_social")
    private Double capitalSocial;

    public Receita() {
    }

    public Receita(String nome, String fantasia, String cnpj, Double capital_social) {
        this.nome = nome;
        this.fantasia = fantasia;
        this.cnpj = cnpj;
        this.capitalSocial = capital_social;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getFantasia() {
        return fantasia;
    }

    public void setFantasia(String fantasia) {
        this.fantasia = fantasia;
    }

    public String getCnpj() {
        return cnpj;
    }

    public void setCnpj(String cnpj) {
        this.cnpj = cnpj;
    }

    public Double getCapitalSocial() {
        return capitalSocial;
    }

    public void setCapitalSocial(Double capitalSocial) {
        this.capitalSocial = capitalSocial;
    }
}

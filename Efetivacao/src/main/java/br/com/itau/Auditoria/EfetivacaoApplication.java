package br.com.itau.Auditoria;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EfetivacaoApplication {

	public static void main(String[] args) {
		SpringApplication.run(EfetivacaoApplication.class, args);
	}

}
